import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;

public class TestReporter extends BaseTest implements ITestListener {

    private static ExtentReports extent = Reporter.createInstance("src/test/java/report/report.html");
    private static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
    private static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();


    public void onTestStart(ITestResult iTestResult) {
        ExtentTest child = parentTest.get().createNode(iTestResult.getMethod().getMethodName());
        test.set(child);

    }

    public void onTestSuccess(ITestResult iTestResult) {
        test.get().pass("Test passed");
    }

    public void onTestFailure(ITestResult iTestResult) {
        String testName = test.get().getClass().getName();
        String path = testName + ".png";
        try {
            takeScreenshot(testName);
            test.get().fail(iTestResult.getThrowable(), MediaEntityBuilder.createScreenCaptureFromPath(path).build()).addScreenCaptureFromPath(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onTestSkipped(ITestResult iTestResult) {
        test.get().skip(iTestResult.getThrowable());
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    public void onStart(ITestContext iTestContext) {
        ExtentTest parent = extent.createTest(getClass().getName());
        parentTest.set(parent);
    }

    public void onFinish(ITestContext iTestContext) {
        extent.flush();
    }

    public ExtentTest getTest() {
        return test.get();
    }


    void takeScreenshot(String str) {

        File scrFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("src/test/java/report/" + str + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        return "src/test/java/report/" + str + ".png";
    }

}