import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GmailPage extends DSL {



    GmailPage(WebDriver driver) {
        super(driver);
    }

    private By enter = By.xpath("/html/body/nav/div/a[2]");
    private By login = By.xpath("//*[@id=\"identifierId\"]");
    private By nextAfterLogin = By.xpath("//*[@id=\"identifierNext\"]/content/span");
    private By password = By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input");
    private By nextAfterPassword = By.xpath("//*[@id=\"passwordNext\"]/content/span");



//    public void openPage(String url) {
//        driver.get(url);
//    }
//
//    public void clickOnEnterBtn() {
//        driver.findElement(enter).click();
//    }
//
//    public void enterLogin(String email) {
//        driver.findElement(login).sendKeys(email);
//    }
//
//    public void clickNextAfterLogin() {
//        driver.findElement(nextAfterLogin).click();
//    }
//
//
//    public void enterPassword(String pass) {
//        driver.findElement(password).sendKeys(pass);
//    }
//
//    public void clickNextAfterPass() {
//        driver.findElement(nextAfterPassword).click();
//    }



//    public GmailPage openPage(String url) {
//        driver.get(url);
//        return this;
//    }
//
//    public GmailPage clickOnEnterBtn() {
//        driver.findElement(enter).click();
//        return this;
//    }
//
//    public GmailPage enterLogin(String email) {
//        driver.findElement(login).sendKeys(email);
//        return this;
//    }
//
//    public GmailPage clickNextAfterLogin() {
//        driver.findElement(nextAfterLogin).click();
//        return this;
//    }
//
//
//    public GmailPage enterPassword(String pass) {
//        driver.findElement(password).sendKeys(pass);
//        return this;
//    }
//
//    public void clickNextAfterPass() {
//        driver.findElement(nextAfterPassword).click();
//    }


    public GmailPage openPage(String url) {
        openHomePage(url);
        return this;
    }

    public GmailPage clickOnEnterBtn() {
        clickOn(enter);
        return this;
    }

    public GmailPage enterLogin(String email) {
        typeText(login, email);
        return this;
    }

    public GmailPage clickNextAfterLogin() {
        clickOn(nextAfterLogin);
        return this;
    }


    public GmailPage enterPassword(String pass) {
        typeText(password, pass);
        return this;
    }

    public void clickNextAfterPass() {
        clickOn(nextAfterPassword);
    }





}
