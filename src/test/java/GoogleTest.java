import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.function.Function;

import static org.testng.Assert.assertEquals;

@Listeners({TestReporter.class})
public class GoogleTest extends BaseTest {


    WebElement webElement;
    private GmailPage gmailPage;







    @Test(dataProvider = "ids", dataProviderClass = TestData.class)
    public void testActions(int id) {

        getDriver().get("https://the-internet.herokuapp.com/hovers");

        sleepFor(5);

        Actions actions = new Actions(getDriver());
//        actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[" + id + "]/img")))
//                .click(driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[" + id + "]/div/a"))).build().perform();

        actions.moveToElement(getDriver().findElement(By.xpath("//*[@id=\"content\"]/div/div[" + id + "]/img"))).build().perform();

        String actual = getDriver().findElement(By.xpath("//*[@id=\"content\"]/div/div[" + id + "]/div/h5")).getText();
        String expected;

        if (id != 2) {
            expected = "name: user" + id;
        } else {
            expected = "name: user" + 5;
        }

        assertEquals(actual, expected, "Test failed");

        sleepFor(5);



    }


    @Test
    public void waitTTT() {

        Wait wait = new FluentWait(getDriver())
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class);


        WebElement elem = (WebElement) wait.until(new Function<WebDriver, WebElement>() {

            @Override
            public WebElement apply(WebDriver driver1) {
                return driver1.findElement(By.id("id"));
            }
        });


        WebElement expectedElem = (new WebDriverWait(getDriver(), 60))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("id")));
    }

    @Test
    public void checkText() {

        String exp = "Sign Up";

        getDriver().get(BASE_URL);

        webElement = getDriver().findElement(By.xpath("//*[@id=\"navbar-collapse\"]/ul/li[2]/a"));


        System.out.println("Found text\n" + webElement.getText());

        System.out.println("attr href\n" + webElement.getAttribute("href"));

        assertEquals(exp, webElement.getText());



    }


    @Test
    public void testGoogle() {

        getDriver().get(BASE_URL);


        System.out.println(getDriver().getTitle());

//        driver.navigate().to("");

//        System.out.println("page source is \n" + driver.getPageSource());

        webElement = getDriver().findElement(By.name("q"));
        webElement.sendKeys("selenium");



//        driver.navigate().to("https://www.seleniumhq.org/");

        webElement = getDriver().findElement(By.name("q"));
        webElement.sendKeys(Keys.RETURN);
//
//        driver.findElement(By.name("btnK")).click();



//        List<WebElement> elements = driver.findElements(By.className("g"));
//
//
//        for (WebElement e : elements) {
//            System.out.println(e.getTest());
//        }

        webElement = getDriver().findElement(By.cssSelector("#rso > div > div > div:nth-child(1) > div > div > div.r > a:nth-child(1) > h3"));
        String attr = webElement.getAttribute("class");

        System.out.println(attr);

//        driver.findElement(By.cssSelector("#rso > div > div > div:nth-child(1) > div > div > div.r > a:nth-child(1) > h3")).click();

//        try {
//            Thread.sleep(2000);
//        } catch (Exception e) {
//
//        }
//
//        driver.navigate().back();

//        String expected = driver.getCurrentUrl();
//
//        Assert.assertEquals("https://www.seleniumhq.org/", expected, "URLs not equals");

//        try {
//            Thread.sleep(2000);
//        } catch (Exception e) {
//
//        }
//
//        driver.navigate().forward();
//
//        try {
//            Thread.sleep(2000);
//        } catch (Exception e) {
//
//        }
//
//        driver.navigate().refresh();
//
//        try {
//            Thread.sleep(2000);
//        } catch (Exception e) {
//
//        }

    }


    @Test
    public void teatGmail() {

        getDriver().get("https://www.google.com/intl/uk/gmail/about/#");
        webElement = getDriver().findElement(By.xpath("/html/body/nav/div/a[2]"));
        webElement.click();
        webElement = getDriver().findElement(By.xpath("//*[@id=\"identifierId\"]"));
        webElement.sendKeys("romaonyshkiv@gmail.com");
        webElement = getDriver().findElement(By.xpath("//*[@id=\"identifierNext\"]/content/span"));
        webElement.click();
        webElement = getDriver().findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input"));
        webElement.sendKeys("sdsf");
        webElement = getDriver().findElement(By.xpath("//*[@id=\"passwordNext\"]/content/span"));
        webElement.click();

        sleepFor(4);

    }


    @Test(dataProvider = "emails", dataProviderClass = TestData.class)
    public void testGmailNew(String email, String password) {

        String url = "https://www.google.com/intl/uk/gmail/about/#";

        gmailPage = new GmailPage(getDriver());

//        gmailPage.openPage("https://www.google.com/intl/uk/gmail/about/#");
//        gmailPage.clickOnEnterBtn();
//        gmailPage.enterLogin("romaonyshkiv@gmail.com");
//        gmailPage.clickNextAfterLogin();
//        gmailPage.enterPassword("asd");
//        gmailPage.clickNextAfterPass();


        gmailPage.openPage(url).clickOnEnterBtn()
                .enterLogin(email).clickNextAfterLogin()
                .enterPassword(password).clickNextAfterPass();

        sleepFor(4);



    }


    @Test
    public void testDropDown() {
        String url = "https://the-internet.herokuapp.com/dropdown";

        DropDownPage dropDownPage = new DropDownPage(getDriver());
        dropDownPage.openPage(url);
        sleepFor(2);
        dropDownPage.selectByCustomValue("Option 2");

        sleepFor(4);
    }





    private void sleepFor(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


}
