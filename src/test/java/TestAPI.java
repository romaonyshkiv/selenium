import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.internal.ResponseSpecificationImpl;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.ResponseSpecification;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class TestAPI {

    @Test
    public void testGetRequest() {

        Gson gson = new Gson();
        RestAssured.baseURI = "http://restcountries.eu/";

        ResponseSpecification specification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();

        RestAssured.responseSpecification = specification;

//        String url = "http://restcountries.eu/rest/v1/name/ukraine";


//        String response =  given().contentType(ContentType.JSON).when().get(Enpoints.endpoint).andReturn().asString();

        String response =  given().when().get(Enpoints.endpoint).then().spec(specification).and().extract().body().asString();


//        given().contentType(ContentType.JSON).when().get(Enpoints.endpoint).andReturn().asInputStream();

        Example[] responseExample = gson.fromJson(response, Example[].class);


        Assert.assertEquals(responseExample[0].getCapital(), "Kiev");
        Assert.assertEquals(responseExample[0].getRegion(), "Europe");



//        Response response = given().contentType(ContentType.JSON).when().get(Enpoints.endpoint);
//
//        Assert.assertEquals(200, response.then().extract().statusCode());


//        JSONArray responseBody = new JSONArray(response.asString());
//
//        String actualVal = responseBody.getJSONObject(0).getString("capital");
//
//        Assert.assertEquals(actualVal, "Kiev");


    }


//    @Test
//    public void testPostRequest() {
//
//        RequestBody body = new RequestBody("Roma", "Onyshkiv", 31);
//
//        Response response = given().contentType(ContentType.JSON).body(body).when().post("http://localhost:8080/create");
//
//
//        int status = response.getStatusCode();
//
//        Assert.assertEquals(status, 201);
//
//
//    }


    @Test
    public void negativePost() {

        RequestBody requestBody = new RequestBody();
        requestBody.setFirstName("Test");

        Response response = given().contentType(ContentType.JSON).body(requestBody).when().post("http://localhost:8080/create");

        int status = response.getStatusCode();

        Assert.assertNotEquals(status, 201);


    }

}
