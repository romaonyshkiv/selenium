import com.aventstack.extentreports.Status;
import lombok.extern.java.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Log
public abstract class DSL extends TestReporter{

    private WebDriver driver;

    DSL(WebDriver driver) {
        this.driver = driver;
    }


    public void openHomePage(String url) {
        log.info("URL '" + url + "' has been opened");
        getTest().log(Status.INFO, "URL '" + url + "' has been opened");
        driver.get(url);
    }

    public void clickOn(By by) {
        log.info("Clicked on '" + by.toString() + "'");
        getTest().log(Status.INFO, "Clicked on '" + by.toString() + "'");
        driver.findElement(by).click();
    }

    public void typeText(By by, String text) {
        log.info("Text '" + text + "' entered into '" + by.toString() + "'");
        getTest().log(Status.INFO, "Text '" + text + "' entered into '" + by.toString() + "'");
        driver.findElement(by).sendKeys(text);
    }

    public List<WebElement> dropdawnValues(By by) {
        return driver.findElements(by);
    }

    public WebElement element(By by) {
        log.info("Element '" + by.toString() + "' has been found");
        getTest().log(Status.INFO, "Element '" + by.toString() + "' has been found");
        return driver.findElement(by);
    }

}
