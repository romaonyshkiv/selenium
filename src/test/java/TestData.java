import org.testng.annotations.DataProvider;

public class TestData {

    @DataProvider(name = "ids")
    public Object[][] getIds() {
        return new Object[][] {
                {1},
                {2},
                {3}
        };
    }

    @DataProvider(name = "emails")
    public Object[][] getEmails() {
        return new Object[][] {
                {"romaonyshkiv@gmail.com", "asdf"}
        };
    }

}
