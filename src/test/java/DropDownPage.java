import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class DropDownPage extends DSL {

    DropDownPage(WebDriver driver) {
        super(driver);
    }


    private By dropDown = By.id("dropdown");
    private By options = By.tagName("option");


    public DropDownPage openPage(String url) {
        openHomePage(url);
        return this;
    }

    public void selectByCustomValue(String value) {



        List<WebElement> elements = dropdawnValues(options);

        for (WebElement e : elements) {
            if (e.getText().equals(value)) {
                System.out.println("found");

                Select select = new Select(element(dropDown));
                select.selectByVisibleText(value);

            }
        }





    }




}
