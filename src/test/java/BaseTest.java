import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    private static ThreadLocal<WebDriver> threadLocalDriver = new ThreadLocal<>();
    public static String BASE_URL;
    Properties properties = new Properties();


    @BeforeTest
    @Parameters({"os"})
    public void setup(String os) throws MalformedURLException {
        WebDriver driver;

        loadProperties();

        System.setProperty("webdriver.chlrome.driver", "/Users/roma/libs/chromedriver");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();

        if(os.equalsIgnoreCase("mac")) {
            capabilities.setPlatform(Platform.MAC);
        } else if (os.equalsIgnoreCase("linux")) {
            capabilities.setPlatform(Platform.LINUX);
        }

        driver = new RemoteWebDriver(new URL("http://10.37.129.2:5555/wd/hub"), capabilities);

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        threadLocalDriver.set(driver);



    }



    public WebDriver getDriver() {
        return threadLocalDriver.get();
    }

//    @BeforeTest
//    public void setup() {
//        loadProperties();
//        WebDriverManager.chromedriver().setup();
//
//
//        ChromeOptions options = new ChromeOptions();
////        options.addArguments("--headless");
//        options.addArguments("--window-size=800,600");
//
//        driver = new ChromeDriver(options);
//
//        driver.manage().timeouts()
//                .implicitlyWait(10, TimeUnit.SECONDS)
//                .pageLoadTimeout(10, TimeUnit.SECONDS)
//                .setScriptTimeout(10, TimeUnit.SECONDS);
//
//    }


    @AfterTest
    public void tearDown() {
        threadLocalDriver.remove();
    }

//    @AfterTest
//    public void tearDown() {
//
//        if (driver != null) {
//            driver.quit();
//        }
//
//    }


    private void loadProperties() {

        String prop = "";
        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("win")){
            prop = "src\\test\\resources\\app.properties";
        } else if (os.contains("mac")) {
            prop = "src/test/resources/app.properties";
        }


        try {
            FileInputStream inputStream = new FileInputStream(new File(prop).getAbsolutePath());
            properties.load(inputStream);
            BASE_URL = properties.getProperty("url");
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
